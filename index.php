<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2 Activity</title>
</head>
<body>
	<!-- Activity 1 - Divisible by Five -->
	<h1>Divisible by Five</h1>
	<?php divisibleByFive(); ?>

	<!-- Activity 2 - Array Manipulation -->
	<h1>Array Manipulation</h1>
	<?php $students = [];
		array_push($students, 'John Smith');
		var_dump($students);
	?>
	</br>
	</br>
	<?php
		echo count($students);
	?>
	</br>
	</br>
	<?php 
		array_push($students, 'Jane Smith');
		var_dump($students);
	?>
	</br>
	</br>
	<?php
		echo count($students);
	?>
	</br>
	</br>
	<?php 
		array_shift($students);
		var_dump($students);
	?>
	</br>
	</br>
	<?php
		echo count($students);
	?>	









</body>
</html>